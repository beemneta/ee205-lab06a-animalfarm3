///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   12_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>
#include <cstdlib>


namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, SILVER,YELLOW,RED,BROWN,WHITE };

class Animal {
public:
   
   Animal();
   ~Animal();

   enum Gender gender;
	string      species;

	virtual const string speak() = 0;
	
	void printInfo();

   static const enum Gender   getRandomGender(); 
   static const enum Color    getRandomColor();
   static const bool          getRandomBool();
   static const float         getRandomWeight( const float from, const float to );
   static const string        getRandomName();     


   string colorName  (enum Color color);
	string genderName (enum Gender gender);
};

} // namespace animalfarm
