///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animalfactory.hpp
/// @version 1.0
///
/// Chooses random animal 
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   12_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "animal.hpp"

namespace animalfarm {

class AnimalFactory {
   public:
      static Animal* getRandomAnimal();
};

}



