///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Beemnet Alemayehu <beemneta@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   12_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animalfactory.hpp"
#include <array>
#include <list>

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 3 " << endl;
   srand(time(NULL)); 
  
   array<Animal*,30> animalArray;
   animalArray.fill( NULL);
   
   for (int i=0;i<25;i++) {
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }   

   cout<<endl<<"Array of Animals: "<<endl;
   cout<<"  Is it empty: "<< boolalpha << animalArray.empty() << endl;
   cout<<"  Number of elements: "<< animalArray.size() << endl;
   cout<<"  Max size: " << animalArray.max_size() << endl;
   
   int count = 0;
   for ( Animal* animal: animalArray ) {
      if (count < 25) {
         count++;
         cout << animal->speak() << endl;
      }
   }
   
   for (int i=0;i<25;i++)
      delete animalArray[i];

   list<Animal*> animalList;
   for (int i = 0; i<25;i++)
      animalList.push_front(AnimalFactory::getRandomAnimal());

   cout<<endl<<"List of Animals: "<<endl;
   cout<<"  Is it empty: "<< boolalpha << animalList.empty() << endl;
   cout<<"  Number of elements: "<< animalList.size() << endl;
   cout<<"  Max size: " << animalList.max_size() << endl;

   for (Animal* animal:animalList) 
      if (count < 50){
         count++;
         cout << animal->speak() << endl;
      }
   
   for (Animal* animal:animalList)
      delete animal;

   cout<<endl;
   
   return 0;
}
